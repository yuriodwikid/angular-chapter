export interface RequestLogin {
    username: string;
    password: string;
  }
  
  export interface ResponseLogin {
    data: {
      jwttoken: string;
      jwtrole: string;
    }
  }

  export interface ResponseRegister {
    jwtrole: string;
  }
  