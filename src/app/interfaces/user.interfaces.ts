export interface User {
  id: number;
  name: string;
  phone: string;
  username: string;
  website: string;
  email: string;
  company: {
    bs: string;
    catchPhrase: string;
    name: string;
  };
  address: {
    city: string;
    street: string;
    suite: string;
    zipcode: string;
    geo: {
      lat: string;
      lng: string;
    };
  };
}
export interface UserList {
  id_user: number;
  username: string;
  fullname: string;
  email: string;
  noHp: string;
}

export interface ResponseUploadPhoto {
  image: string;
}
