import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminGuard } from './guards/admin.guard';
import { UserGuard } from './guards/user.guard';
import { LoginComponent } from './pages/login/login.component';
import { PertaminiComponent } from './pages/pertamini/pertamini.component';
import { PlaygroundComponent } from './pages/playground/playground.component';
import { RegisterComponent } from './pages/register/register.component';
import { UsersComponent } from './pages/users/users.component';


const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'playground',
    component: PlaygroundComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'pertamini',
    component: PertaminiComponent
  },
  {
    path: 'users',
    component: UsersComponent, canActivate: [UserGuard],
  },
  {
    path: 'admin',
    loadChildren: () =>
      import('./pages/admin/admin.module').then((m) => m.AdminModule),
      // canLoad: [AdminGuard],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }