import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PertaminiComponent } from './pertamini.component';

describe('PertaminiComponent', () => {
  let component: PertaminiComponent;
  let fixture: ComponentFixture<PertaminiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PertaminiComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PertaminiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
