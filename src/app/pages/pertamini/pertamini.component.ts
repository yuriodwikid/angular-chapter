import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'pertamini',
  templateUrl: './pertamini.component.html',
  styleUrls: ['./pertamini.component.scss']
})
export class PertaminiComponent {
  // bensinForm:FormGroup; 

  tipe = "";
  harga = 0;

  liter = 0;
  bayar = 0;

  harga_bayar = 0;
  kembali = 0;

  constructor(private dataService: DataService) {
    this.harga = this.dataService.baseHarga
  }

  // constructor(fb: FormBuilder) { 
  //   this.bensinForm = fb.group({
  //     liter: 0,
  //     bayar: 0
  //   });
  // }  
  
  doClick(t: any) {
    this.tipe = t

    if (t == "Pertamax") {
      this.harga = 15000
    } else if (t == "Pertalite") {
      this.harga = 10000
    } else if (t == "Solar") {
      this.harga = 8000
    } else {
      this.harga = 20000
    }
    console.log(t)
  }

  doHitung() {
    // this.liter = this.bensinForm.get('liter')?.value;
    // console.log(this.liter)
    // this.bayar = this.bensinForm.get('bayar')?.value;
    // console.log(this.bayar)

    // this.harga_bayar = this.harga * this.liter
    // this.kembali = this.bayar - this.harga_bayar
  }

}