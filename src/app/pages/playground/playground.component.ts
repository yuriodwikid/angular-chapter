import { Component } from '@angular/core';

@Component({
  selector: 'app-playground',
  templateUrl: './playground.component.html',
  styleUrls: ['./playground.component.scss']
})
export class PlaygroundComponent {
  title = 'DAY1_INTRO';

  name = 'rio';
  age = 19;
  status = false;

  showData = false;

  nomor = 1;

  person = {
    title: 'test a',
    name: 'yuri',
    age: 10,
    status: true
  }
}
