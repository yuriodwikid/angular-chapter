import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  // username = '';
  // password = '';
  showAlert = 'alert alert-info';

  formLogin: FormGroup;

  error = '';

  constructor(
    private authService: AuthService,
    private router: Router,
    private formBuilder: FormBuilder
  ) {
    this.formLogin = this.formBuilder.group({
      username: ['', [Validators.required, Validators.minLength(2)]],
      password: ['', [Validators.required]],
    });
  }

  get errorControl() {
    return this.formLogin.controls;
  }

  doLogin() {
    console.log(this.formLogin);
    const payload = {
      username: this.formLogin.value.username,
      password: this.formLogin.value.password,
    };
    this.authService.login(payload).subscribe(
      (response) => {
        console.log(response);
        // if (response.token) {
        //   this.router.navigate(['/playground']);
        // }
        // alert(response.data.jwtrole);
        if (response.data.jwtrole === 'admin') {
          localStorage.setItem('token', response.data.jwttoken);
          this.router.navigate(['/admin/dashboard']);
          Swal.fire({
            title: 'Success!',
            text: 'Anda berhasil mengubah password user',
            icon: 'success',
            confirmButtonText: 'OK'
          })
        }
        if (response.data.jwtrole === 'user') {
          Swal.fire({
            title: 'Gagal!',
            text: 'Anda bukan admin',
            icon: 'warning',
            confirmButtonText: 'OK'
          })
          this.router.navigate(['/login']);
        } else {
          this.showAlert = 'alert alert-warning';
          console.log("You don't have permission to enter this page");
        }
      },
      (error) => {
        console.log(error);
        // alert(error.error.message);
        Swal.fire({
          title: 'Gagal!',
          text: 'Username dan Password tidak terdaftar',
          icon: 'error',
          confirmButtonText: 'OK'
        })
        this.error = error.error.message;
      }
    );
  }
}
