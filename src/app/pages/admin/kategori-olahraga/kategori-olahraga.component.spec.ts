import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KategoriOlahragaComponent } from './kategori-olahraga.component';

describe('KategoriOlahragaComponent', () => {
  let component: KategoriOlahragaComponent;
  let fixture: ComponentFixture<KategoriOlahragaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KategoriOlahragaComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(KategoriOlahragaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
