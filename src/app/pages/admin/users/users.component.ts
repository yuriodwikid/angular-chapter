import { Component, TemplateRef } from '@angular/core';
import { UserList } from 'src/app/interfaces/user.interfaces';
import { UsersService } from 'src/app/services/users.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { map, switchMap } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent {
  users!: UserList[];
  modalRef?: BsModalRef;

  photo!: string;
  photoFile!: File;
  // formAdd: FormGroup;
  // users: []

  userDetail: UserList = {
    id_user: 0,
    fullname: '',
    username: '',
    email: '',
    noHp: '',
  };

  constructor(
    private userServices: UsersService,
    private modalService: BsModalService,
    private authService: AuthService,
    private formBuilder: FormBuilder
  ) {
    this.userServices.listUsers().subscribe((response) => {
      console.log(response);
      this.users = response;
    });

    // this.formAdd = this.formBuilder.group({
    //   username: [''],
    //   password: [''],
    //   fullname: [''],
    //   email: [''],
    // });
  }

  // get errorControl() {
  //   return this.formAdd.controls;
  // }

  // openModal(template: TemplateRef<any>) {
  //   this.modalRef = this.modalService.show(template);
  // }

  showPreview(event: any) {
    if (event) {
      const file = event.target.files[0];
      this.photoFile = file;
      const reader = new FileReader();
      reader.onload = () => {
        this.photo = reader.result as string;
      };
      reader.readAsDataURL(file);
    }
  }

  // doAddUser() {
  //   Swal.fire({
  //     title: 'Successfully Add The Data!',
  //     text: 'You have successfully add 1 user data',
  //     icon: 'success',
  //     confirmButtonColor: '#0046e6',
  //     confirmButtonText: 'OK',
  //   });

  //   this.userServices
  //     .uploadPhoto(this.photoFile)
  //     .pipe(
  //       switchMap((val) => {
  //         const payload = {
  //           username: this.formAdd.value.username,
  //           password: this.formAdd.value.password,
  //           email: this.formAdd.value.email,
  //           fullname: this.formAdd.value.fullname,
  //           // photo: val.data,
  //         };
  //         return this.authService.register(payload).pipe(map((val) => val));
  //       })
  //     )
  //     .subscribe((response) => console.log(response));
  // }

  // doCheckDetail(data: UserList) {
  //   console.log(data);
  //   this.userDetail = data;
  // }
}