import { Component, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { map, Subject, switchMap, takeUntil } from 'rxjs';
import { KategoriList } from 'src/app/interfaces/kategori.interface';
import { KategoriService } from 'src/app/services/kategori.service';

@Component({
  selector: 'app-kategori',
  templateUrl: './kategori.component.html',
  styleUrls: ['./kategori.component.scss'],
})
export class KategoriComponent {
  kategori!: KategoriList[];
  formAdd: FormGroup;
  modalRef?: BsModalRef;
  gambarKategori!: string;
  photoFile!: File;
  refresh = new Subject<void>();

  constructor(
    private kategoriServices: KategoriService,
    private modalService: BsModalService,
    private formBuilder: FormBuilder
  ) {
    this.loadData();
    this.formAdd = this.formBuilder.group({
      namaKategori: ['', [Validators.required]],
      gambarKategori: ['', [Validators.required]],
    });
  }

  loadData() {
    this.kategoriServices
      .listKategori()
      .pipe(takeUntil(this.refresh))
      .subscribe((response) => {
        console.log(response);
        this.kategori = response;
      });
  }

  get errorControl() {
    return this.formAdd.controls;
  }
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }
  showPreview(event: any) {
    if (event) {
      const file = event.target.files[0];
      this.photoFile = file;
      const reader = new FileReader();
      reader.onload = () => {
        this.gambarKategori = reader.result as string;
      };
      reader.readAsDataURL(file);
    }
  }
  doAddKategori() {
    this.kategoriServices
      .uploadPhotoKategori(this.photoFile)
      .pipe(
        switchMap((val) => {
          const payload = {
            namaKategori: this.formAdd.value.namaKategori,
            gambarKategori: val.data,
          };
          return this.kategoriServices
            .kategori(payload)
            .pipe(map((val) => val));
        })
      )
      .subscribe((response) => {
        console.log(response);
        this.loadData();
      });
  }
}
