import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  // title = 'ng-batch-four'

  // name = 'yuri'
  // age = 18
  // status = false

  // person = {
  //   title: 'Test A',
  //   name: 'rio',
  //   age: 0,
  //   status: true
  // }

  // datas = [1,2,3]

  // constructor() {
  //   this.name = 'darmawan'
  //   this.age = 21
  // }

  // onCallBack(ev: any){
  //   console.log(ev);
  // }
}
