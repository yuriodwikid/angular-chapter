import { Injectable } from '@angular/core';
import { DataService } from './data.service';
import { HttpClient } from '@angular/common/http';
import { RequestLogin, ResponseLogin } from 'src/app/interfaces/auth.interface';
import { BehaviorSubject, Observable, tap } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private baseApi = 'http://localhost:8080';

  public isAdmin = new BehaviorSubject<boolean>(false);
  public isUser = new BehaviorSubject<boolean>(false);

  constructor(private httpClient: HttpClient) {}
  login(payload: RequestLogin): Observable<ResponseLogin> {
    return this.httpClient.post<ResponseLogin>(
      this.baseApi + '/login',payload)
      .pipe(
        tap((val) => {
          console.log(val);
          if (val.data.jwtrole == 'admin') {
            this.isAdmin.next(true);
          } else if (val.data.jwtrole === 'user') {
            this.isUser.next(true);
          }
        })
      );
  }
  register(payload: {
    username: string;
    fullname: string;
    email: string;
    password: string;
    
  }) {
    return this.httpClient.post(this.baseApi + '/register', payload);
  }
}
