import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { KategoriList } from 'src/app/interfaces/kategori.interface';
import { User } from 'src/app/interfaces/user.interfaces';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  public baseHarga = 20000;
  private baseApi = 'https://jsonplaceholder.typicode.com';
  constructor(private httpClient: HttpClient) {}

  getUser(): Observable<User[]> {
    return this.httpClient.get<User[]>(this.baseApi + '/users');
  }
  private baseApiFP = 'http://localhost:8080';
  // getUsers(): Observable<Users[]> {
  //   return this.httpClient.get<Users[]>(this.baseApiFP + '/lihat-user');
  // }

  // getKategori(): Observable<Kategori[]> {
  //   return this.httpClient.get<Kategori[]>(this.baseApiFP + '/lihat-kategori');
  // }
}
